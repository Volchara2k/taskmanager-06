# TaskManager-05

# О проекте
**Менеджер задач**

### Стек технологий:
Java SE/Spring/SPRING REST/SPRING CLOUD/SPRING TESTING/SPRING SECURITY/
SPRING SOAP/SPRING AOP/SPRING IOC/Docker/Git/MySql/PostgreSql/Maven 3/GRADLE/
HIBERNATE

### Программные требования:
- JDK 1.8;
- MS Windows 10 x64.

### Аппаратные требования:
- Процессор: Intel® Dual-Core 2.4 GHz - или аналог;
- Оперативная память: 1.5 GB; 
- Место на диске: менее 5 МБ.

### Сведения о разработчике:
**ФИО**: Волков Валерий Сергеевич

**Электронная почта**: volkov.valery2013@yandex.ru

### Команда для сборки приложения:
```bash
mvn clean install
```

### Команда для запуска приложения:
```bash
java -jar ./taskmanager.jar
```
### Текущая сборка CI / CD:
https://gitlab.com/Volchara2k/taskmanager-05/-/pipelines

## Скриншоты:
https://drive.google.com/drive/folders/1amDrOh3iuEg9oJSrgO2PIl3mbp8X-5QX?usp=sharing