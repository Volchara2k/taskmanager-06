package ru.renessans.jvschool.volkov.task.manager.constant;

public interface ArgConst {

    String NO_ARG = "Входной аргумент отсутствует!";

    String HELP_ARG = "help";

    String VERSION_ARG = "version";

    String ABOUT_ARG = "about";

    String FORMAT_UNKNOWN_MSG = "Неизвестный аргумент: %s";

    String FORMAT_HELP_MSG = "%s - для вывода версии программы; \n" +
            "%s - для информации о разработчике; \n" +
            "%s - для вывода списка команд.";

    String FORMAT_ABOUT_MSG = "%s - разработчик; \n%s - почта.";

    String VERSION_MSG = "Версия: 1.0.0";

    String DEVELOPER = "Valery Volkov";

    String DEVELOPER_MAIL = "volkov.valery2013@yandex.ru";

}
